/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import hui.AlgoHUPMiner;
/**
 *
 * @author Dwi Asana
 */
public class HUIFrame extends javax.swing.JFrame {
    boolean kategori;
    public HUIFrame(boolean kategori) {
        this.kategori=kategori;
        initComponents();
        this.getContentPane().setBackground(Color.white);
        setTabel();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        txtcarifile = new javax.swing.JTextField();
        btncarifile1 = new javax.swing.JButton();
        btnAnalisisbyItem = new javax.swing.JButton();
        txtcarifile1 = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        datatabel = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(32, 47, 95));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setBackground(new java.awt.Color(32, 47, 95));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/settings.png"))); // NOI18N
        jLabel4.setToolTipText("Alat Bantuan");
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jLabel4.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel5.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 40, 30));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/info.png"))); // NOI18N
        jLabel6.setToolTipText("INFO");
        jLabel6.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jLabel6.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel5.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 40, 30));

        getContentPane().add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 50, 720));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Segoe UI Light", 0, 36)); // NOI18N
        jLabel2.setText("PolKon 1.0");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(24, 22, -1, -1));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/data (1).png"))); // NOI18N
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(920, 10, 150, 140));

        jLabel7.setFont(new java.awt.Font("Segoe UI Light", 0, 12)); // NOI18N
        jLabel7.setText("Aplikasi Analisis Pola Belanja Konsumen ");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(24, 76, -1, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1110, 150));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/open-archive.png"))); // NOI18N
        jLabel3.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jLabel3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel4.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, -1, 30));
        jPanel4.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 40, 260, 23));

        txtcarifile.setBorder(null);
        jPanel4.add(txtcarifile, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 10, 260, 31));

        btncarifile1.setText("Cari File");
        btncarifile1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncarifile1ActionPerformed(evt);
            }
        });
        jPanel4.add(btncarifile1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 10, -1, 34));

        btnAnalisisbyItem.setText("Analisis Data");
        btnAnalisisbyItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnalisisbyItemActionPerformed(evt);
            }
        });
        jPanel4.add(btnAnalisisbyItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 10, -1, 34));

        txtcarifile1.setBorder(null);
        jPanel4.add(txtcarifile1, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 10, 140, 30));
        jPanel4.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 40, 140, 23));

        jLabel1.setFont(new java.awt.Font("Segoe UI Light", 0, 12)); // NOI18N
        jLabel1.setText("Minimum Profit");
        jPanel4.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 20, -1, -1));

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, 1110, 60));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(44, 0, 1110, 210));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        datatabel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "No Transaksi", "Item", "Bobot Transaksi"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(datatabel);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1100, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 507, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 20, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(44, 213, 1110, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btncarifile1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncarifile1ActionPerformed
        // TODO add your handling code here:
        JFileChooser jfc = new JFileChooser();
        jfc.showOpenDialog(null);
        File file = jfc.getSelectedFile();
        String dir = file.getAbsolutePath();
        txtcarifile.setText(dir);
    }//GEN-LAST:event_btncarifile1ActionPerformed

    private void btnAnalisisbyItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnalisisbyItemActionPerformed
        AlgoHUPMiner algojo = new AlgoHUPMiner();
        try {
             String inp,outp;
             
             inp = txtcarifile.getText();
             outp="hasil.txt";
             algojo.runAlgorithm(inp, outp, 1, 1,this.kategori);
             showresult(outp);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        
    }//GEN-LAST:event_btnAnalisisbyItemActionPerformed
    public void showresult(String ouputfile) throws FileNotFoundException{
        DefaultTableModel model = new DefaultTableModel();
        datatabel.setModel(model);
        //menghapus seluruh data pada table
        model.getDataVector().removeAllElements();
        //memberi tahu bahwa data telah kosong
        model.fireTableDataChanged();
        model.addColumn("No");
        model.addColumn("Itemset");
        model.addColumn("Keterangan");   
        BufferedReader output = null;
        String thisLine;
        Object[] obj = new Object[3];
        output = new BufferedReader(new InputStreamReader(new FileInputStream(new File(ouputfile))));
        int norut = 1;
        try {
            while ((thisLine = output.readLine()) != null) {
                obj[0] = Integer.toString(norut) ;
                obj[1] = thisLine; 
               obj[2] = " ";
                model.addRow(obj);
                norut++;
            }
        } catch (IOException ex) {
            Logger.getLogger(HUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   
    public void setTabel(){
       
             
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
            /* Set the Nimbus look and feel */
            //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        try {
         //   javax.swing.UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
           javax.swing.UIManager.setLookAndFeel("com.jtattoo.plaf.fast.FastLookAndFeel");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(HUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(HUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(HUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(HUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
            //</editor-fold>
            //</editor-fold>
        
        
       // getcon
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                HUIFrame oMainFrame = new HUIFrame(false);
                oMainFrame.pack();
                oMainFrame.setLocationRelativeTo(null);  
                oMainFrame.setVisible(true);               
            }
        });
        
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnalisisbyItem;
    private javax.swing.JButton btncarifile1;
    private javax.swing.JTable datatabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField txtcarifile;
    private javax.swing.JTextField txtcarifile1;
    // End of variables declaration//GEN-END:variables
}
