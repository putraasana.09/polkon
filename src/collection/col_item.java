package collection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DwiPutraAsana
 */
public class col_item {
    final Map<String, String> mapItem = new HashMap<String, String>();
    BufferedReader reader;
    String line;
//    public col_item() throws IOException {
//       
//    }
    public void loadMapItem() throws IOException{
     try {
            String[]nextline;
            String namaitem="";
            Reader reader = Files.newBufferedReader(Paths.get("D:/Penelitian/polkon/data_barang.csv"));
            CSVReader csvReader = new CSVReader(reader,',','\'',1);
            while ((nextline=csvReader.readNext())!= null) {
                if (nextline != null) {
                    namaitem=nextline[1].replace("'", "");
                    mapItem.put(nextline[0],namaitem);
               }
            }             
        } catch (FileNotFoundException ex) {
            Logger.getLogger(col_item.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public Map getMapItem(){
        return this.mapItem;
    }   
    
    public String getDeskripsiItemSet(Map mapItem, String key){
        String deskripsi="";
        
        String[] itemset= key.split(":");
        String[] item= itemset[0].split(" ");
        for(String itemString : item){ 
            deskripsi += (String) mapItem.get(itemString)+" ";
        }
        deskripsi +=":"+itemset[1];
        return deskripsi;
    }
}
