/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import hui.AlgoHUPMiner;
/**
 *
 * @author Dwi Asana
 */
public class MainFrame extends javax.swing.JFrame {
    
    public MainFrame() {
        initComponents();
        this.getContentPane().setBackground(Color.white);
        setTabel();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnFPKategori = new javax.swing.JButton();
        btnFPItem = new javax.swing.JButton();
        btnHuiItem = new javax.swing.JButton();
        btnHuiKategori = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(32, 47, 95));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel5.setBackground(new java.awt.Color(32, 47, 95));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/settings.png"))); // NOI18N
        jLabel4.setToolTipText("Alat Bantuan");
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jLabel4.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel5.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, 40, 30));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/info.png"))); // NOI18N
        jLabel6.setToolTipText("INFO");
        jLabel6.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jLabel6.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel5.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 40, 30));

        getContentPane().add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 50, 410));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/data (1).png"))); // NOI18N
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 0, 150, 140));

        jLabel7.setFont(new java.awt.Font("Segoe UI Light", 0, 12)); // NOI18N
        jLabel7.setText("Aplikasi Analisis Pola Belanja Konsumen ");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(24, 76, -1, -1));

        jLabel3.setFont(new java.awt.Font("Segoe UI Light", 0, 36)); // NOI18N
        jLabel3.setText("PolKon 1.0");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(24, 22, -1, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 650, 150));

        btnFPKategori.setText("Kategori");
        btnFPKategori.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFPKategoriActionPerformed(evt);
            }
        });
        jPanel1.add(btnFPKategori, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 210, 100, 80));

        btnFPItem.setText("Item");
        btnFPItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFPItemActionPerformed(evt);
            }
        });
        jPanel1.add(btnFPItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, 100, 80));

        btnHuiItem.setText("Item");
        btnHuiItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHuiItemActionPerformed(evt);
            }
        });
        jPanel1.add(btnHuiItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 210, 100, 80));

        btnHuiKategori.setText("Kategori");
        btnHuiKategori.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHuiKategoriActionPerformed(evt);
            }
        });
        jPanel1.add(btnHuiKategori, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 210, 100, 80));

        jLabel2.setFont(new java.awt.Font("Segoe UI Light", 0, 24)); // NOI18N
        jLabel2.setText("High Utility Itemset");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 170, -1, -1));

        jLabel8.setFont(new java.awt.Font("Segoe UI Light", 0, 24)); // NOI18N
        jLabel8.setText("Frequent Itemset");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 170, 210, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(44, 0, 650, 410));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFPKategoriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFPKategoriActionPerformed
        try {
            FPFrame oFPFrame = new FPFrame(true);
            oFPFrame.pack();
            oFPFrame.setLocationRelativeTo(null);  
            oFPFrame.setVisible(true);
        } catch (Exception e) {
            System.out.println(e.toString()); 
        }        // TODO add your handling code here:
    }//GEN-LAST:event_btnFPKategoriActionPerformed

    private void btnFPItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFPItemActionPerformed
        // TODO add your handling code here:
         try {
            FPFrame oFPFrame = new FPFrame(false);
            oFPFrame.pack();
            oFPFrame.setLocationRelativeTo(null);  
            oFPFrame.setVisible(true);
        } catch (Exception e) {
            System.out.println(e.toString()); 
        }
    }//GEN-LAST:event_btnFPItemActionPerformed

    private void btnHuiItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHuiItemActionPerformed

        // TODO add your handling code here:
        try {
            HUIFrame oHuiFrame = new HUIFrame(false);
            oHuiFrame.pack();
            oHuiFrame.setLocationRelativeTo(null);  
            oHuiFrame.setVisible(true);
        } catch (Exception e) {
            System.out.println(e.toString()); 
        }
    }//GEN-LAST:event_btnHuiItemActionPerformed

    private void btnHuiKategoriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHuiKategoriActionPerformed
        // TODO add your handling code here:
         try {
            HUIFrame oHuiFrameKategori = new HUIFrame(true);
            oHuiFrameKategori.pack();
            oHuiFrameKategori.setLocationRelativeTo(null);  
            oHuiFrameKategori.setVisible(true);
        } catch (Exception e) {
            System.out.println(e.toString()); 
        }
    }//GEN-LAST:event_btnHuiKategoriActionPerformed
     
    public void setTabel(){
       
             
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
            /* Set the Nimbus look and feel */
            //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        try {
         //   javax.swing.UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
           javax.swing.UIManager.setLookAndFeel("com.jtattoo.plaf.fast.FastLookAndFeel");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(HUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(HUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(HUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(HUIFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
            //</editor-fold>
            //</editor-fold>
        
        
       // getcon
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MainFrame oMainFrame = new MainFrame();
                oMainFrame.pack();
                oMainFrame.setLocationRelativeTo(null);  
                oMainFrame.setVisible(true);               
            }
        });
        
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFPItem;
    private javax.swing.JButton btnFPKategori;
    private javax.swing.JButton btnHuiItem;
    private javax.swing.JButton btnHuiKategori;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    // End of variables declaration//GEN-END:variables
}
