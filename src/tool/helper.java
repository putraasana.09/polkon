package tool;
import java.util.regex.Pattern;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DwiPutraAsana
 */
public class helper {
     private final static Pattern LTRIM = Pattern.compile("^\\s+");
     private final static Pattern RTRIM = Pattern.compile("\\s+$");

        public static String ltrim(String s) {
            return LTRIM.matcher(s).replaceAll("");
        }
        
        public static String rtrim(String s) {
            return RTRIM.matcher(s).replaceAll("");
        }
        
}
