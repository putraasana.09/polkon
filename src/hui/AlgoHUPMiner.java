/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hui;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import hui.ca.tools.MemoryLogger;
import java.util.regex.Pattern;
import static tool.helper.*;
public class AlgoHUPMiner {
    	/** the time at which the algorithm started */
	public long startTimestamp = 0;  
	/** the time at which the algorithm ended */
	public long endTimestamp = 0; 
	/** the number of high-utility itemsets generated */
	public int huiCount =0; 
	/** Map to remember the TWU of each item */
	Map<String, Integer> mapItemToTWU;
	/** writer to write the output file  */
	BufferedWriter writer = null;  
	/** the number of utility-list that was constructed */
	private int joinCount;
	/** the number of utility-list constructions that were aborted by LA-Prune */
	private int partialJoinCount;
	/** the number of transactions in this database */
	private int transactionCount;
	/** the number of partitions k */
	private int k;
	/** the size of a partition as a number of transactions */
	private int partitionSize;
	/** buffer for storing the current itemset that is mined when performing mining
	* the idea is to always reuse the same buffer to reduce memory usage. */
	final int BUFFERS_SIZE = 200;
	private String[] itemsetBuffer = null;
	/** this class represent an item and its utility in a transaction */
	class Pair{
		String item="";
		int utility = 0;
	}
        
        /**
	 * Default constructor
	 */
       
        
	public AlgoHUPMiner() {
	}
	/**
	 * Run the algorithm
	 * @param input the input file path
	 * @param output the output file path
	 * @param minUtility the minimum utility threshold
	 * @param k the number of partitions
	 * @throws IOException exception if error while writing the file
	 */
        public void runAlgorithm(String input, String output, int minUtility, int k, boolean iskategori) throws IOException {
            // reset maximum
//            helper Helper = new helper();
            MemoryLogger.getInstance().reset();
            // initialize the buffer for storing the current itemset
            itemsetBuffer = new String[BUFFERS_SIZE];
            startTimestamp = System.currentTimeMillis();
            writer = new BufferedWriter(new FileWriter(output));
            //  We create a  map to store the TWU of each item
            mapItemToTWU = new HashMap<String, Integer>();
            // We scan the database a first time to calculate the TWU of each item.
            BufferedReader myInput = null;
            String thisLine;
            transactionCount = 0;
            try {
                // prepare the object for reading the file
                myInput = new BufferedReader(new InputStreamReader( new FileInputStream(new File(input))));
                
                // for each line (transaction) until the end of file
                while ((thisLine = myInput.readLine()) != null){
                     // if the line is  a comment, is  empty or is a
                    // kind of metadata
                    if (thisLine.isEmpty() == true ||
                        thisLine.charAt(0) == '#' || thisLine.charAt(0) == '%' || 
                        thisLine.charAt(0) == '@') {
                            continue;
                    }
                    // split the transaction according to the : separator
                    String split[] = thisLine.split(":");
                    for(int s=0; s <split.length; s++){
                        split[s]=rtrim(split[s]);
                        split[s]=ltrim(split[s]);
                    }
                    // the first part is the list of items
                    // cek yg diiminta berdasarkan item atau kelompok item
                    String  items[];
                    if (iskategori==true) {
                        items = split[3].split(" ");
                    } else {
                        items = split[0].split(" ");
                    }
                    // the second part is the transaction utility
                    double tudata = Double.parseDouble(split[1]);
                    int transactionUtility = (int) tudata;//Integer.parseInt(split[1]);
                    
                    for(int i=0; i <items.length; i++){
                        // convert item to integer
                        String item = items[i];//Integer.parseInt(items[i]);
                        // get the current TWU of that item
                        Integer twu = mapItemToTWU.get(item);
                        // add the utility of the item in the current transaction to its twu
                        twu = (twu == null)? 
                                        transactionUtility : twu + transactionUtility;
                        mapItemToTWU.put(item, twu);
                        
                    }
                    transactionCount++;
                }
            } catch (Exception e) {
                 // catches exception if error while reading the input file
		e.printStackTrace();
            }finally {
                if(myInput != null){
                        myInput.close();
                }
	    }
            //===== BEGIN HUP-Miner
            // save the desired number of partitions
            this.k = k;

            if(k > transactionCount) {
                this.partitionSize = transactionCount;
                this.k = 1;
            }else {
                this.partitionSize = transactionCount / this.k;
                this.k = (int) Math.ceil(transactionCount / (double) partitionSize);
            }
            //===== END HUP-Miner
             // CREATE A LIST TO STORE THE UTILITY LIST OF ITEMS WITH TWU  >= MIN_UTILITY.
            List<UtilityListHUP> listOfUtilityLists = new ArrayList<UtilityListHUP>();
            // CREATE A MAP TO STORE THE UTILITY LIST FOR EACH ITEM.
            // Key : item    Value :  utility list associated to that item
            Map<String, UtilityListHUP> mapItemToUtilityList = new HashMap<String, UtilityListHUP>();
            // For each item
            for(String item: mapItemToTWU.keySet()){
                    // if the item is promising  (TWU >= minutility)
                    if(mapItemToTWU.get(item) >= minUtility){
                            // create an empty Utility List that we will fill later.
                            UtilityListHUP uList = new UtilityListHUP(item, this.k);
                            mapItemToUtilityList.put(item, uList);
                            // add the item to the list of high TWU items
                            listOfUtilityLists.add(uList); 
                    }
            }
            
            // SORT THE LIST OF HIGH TWU ITEMS IN ASCENDING ORDER
            Collections.sort(listOfUtilityLists, new Comparator<UtilityListHUP>(){
                public int compare(UtilityListHUP o1, UtilityListHUP o2) {
                        // compare the TWU of the items
                    return compareItems(o1.item, o2.item);
                    
                }});
            
            // SECOND DATABASE PASS TO CONSTRUCT THE UTILITY LISTS 
            // OF 1-ITEMSETS  HAVING TWU  >= minutil (promising items)
            try {
                // prepare object for reading the file
                myInput = new BufferedReader(new InputStreamReader(new FileInputStream(new File(input))));
                // variable to count the number of transaction
                int tid =0;
                // for each line (transaction) until the end of file
                while ((thisLine = myInput.readLine()) != null) {
                    // if the line is  a comment, is  empty or is a
                    // kind of metadata
                    if (thisLine.isEmpty() == true ||
                                    thisLine.charAt(0) == '#' || thisLine.charAt(0) == '%'
                                                    || thisLine.charAt(0) == '@') {
                        continue;
                    }
                    // split the line according to the separator
                    String split[] = thisLine.split(":");
                    for(int s=0; s <split.length; s++){
                        split[s]=rtrim(split[s]);
                        split[s]=ltrim(split[s]);
                    }
                    // get the list of items
                    String  items[];
                    if (iskategori==true) {
                        items = split[3].split(" ");
                    } else {
                        items = split[0].split(" ");
                    }
                    // get the list of utility values corresponding to each item
                    // for that transaction
                    String utilityValues[] = split[2].split(" ");
                    // Copy the transaction into lists but 
                    // without items with TWU < minutility
                    int remainingUtility =0;
                    // Create a list to store items
                    List<Pair> revisedTransaction = new ArrayList<Pair>();
                    for(int i=0; i <items.length; i++){
                        /// convert values to integers
                        Pair pair = new Pair();
                        pair.item = items[i];//Integer.parseInt(items[i]);
                        double uitem = Double.parseDouble(utilityValues[i]);
                        pair.utility = (int) uitem;//Integer.parseInt(utilityValues[i]);
                        // if the item has enough utility
                        if(mapItemToTWU.get(pair.item) >= minUtility){
                                // add it
                                revisedTransaction.add(pair);
                                remainingUtility += pair.utility;
                        }
                    }
                    Collections.sort(revisedTransaction, new Comparator<Pair>(){
                    public int compare(Pair o1, Pair o2) {
                            return compareItems(o1.item, o2.item);
                    }});
                    // for each item left in the transaction
                    for(Pair pair : revisedTransaction){
                            // subtract the utility of this item from the remaining utility
                            remainingUtility = remainingUtility - pair.utility;

                            // get the utility list of this item
                            UtilityListHUP utilityListOfItem = mapItemToUtilityList.get(pair.item);

                            // Add a new Element to the utility list of this item corresponding to this transaction
                            Element element = new Element(tid, pair.utility, remainingUtility);

                            utilityListOfItem.addElement(element, partitionSize);
                    }
                    tid++; // increase tid number for next transaction
                }
            } catch (Exception e) {
                // to catch error while reading the input file
                e.printStackTrace();
            }finally {
			if(myInput != null){
				myInput.close();
			}
	    }
            // check the memory usage
            MemoryLogger.getInstance().checkMemory();

            // Mine the database recursively
            hupMiner(itemsetBuffer, 0, null, listOfUtilityLists, minUtility);

            // check the memory usage again and close the file.
            MemoryLogger.getInstance().checkMemory();
            // close output file
            writer.close();
            // record end time
            endTimestamp = System.currentTimeMillis();
            printStats();
        }
        private int compareItems(String item1, String item2) {
		int compare = mapItemToTWU.get(item1) - mapItemToTWU.get(item2);
		// if the same, use the lexical order otherwise use the TWU
		return (compare == 0)? item1.compareTo(item2) :  compare;
	}
        private void hupMiner(String [] prefix, 
            int prefixLength, UtilityListHUP pUL, List<UtilityListHUP> ULs, int minUtility)
            throws IOException {
		// For each extension X of prefix P
		for(int i=0; i< ULs.size(); i++){
                    UtilityListHUP X = ULs.get(i);
                    // If pX is a high utility itemset.
                    // we save the itemset:  pX 
                    if(X.sumIutils >= minUtility){
                        // save to file
                        writeOut(prefix, prefixLength, X.item, X.sumIutils);
                    }
                    // If the sum of the remaining utilities for pX
                    // is higher than minUtility, we explore extensions of pX.
                    // (this is the pruning condition)
                    if(X.sumIutils + X.sumRutils >= minUtility){
                            // This list will contain the utility lists of pX extensions.
                        List<UtilityListHUP> exULs = new ArrayList<UtilityListHUP>();
                        // For each extension of p appearing
                        // after X according to the ascending order
                        for(int j=i+1; j < ULs.size(); j++){
                            UtilityListHUP Y = ULs.get(j);
                            //=== BEGIN HUP-Miner
                            if(puPrune(X, Y, minUtility)) {
                                    continue;
                            }
                            //=== END HUP-Miner
                            // we construct the extension pXY 
                            // and add it to the list of extensions of pX
                            UtilityListHUP pul = construct(pUL, X, Y, minUtility);
                            //=== BEGIN HUP-Miner
                            if(pul != null) {
                            //=== END HUP-Miner
                                    exULs.add(pul);
                                    joinCount++;
                            }else {
                                    partialJoinCount++;
                            }
                        }
                        // We create new prefix pX
                        itemsetBuffer[prefixLength] = X.item;
                        // We make a recursive call to discover all itemsets with the prefix pXY
                        hupMiner(itemsetBuffer, prefixLength+1, X, exULs, minUtility); 
                    }
		}
	}
        //=== BEGIN HUP-Miner
	/** This method check if the combination pXY of two itemsets
	 * pX and pY and extensions of pXY 
	 * should be pruned according to the PU-Prune property.
	 * @param X  the utility-list of item pX
	 * @param Y  the utility-list of itemset pY
	 * @param minUtil the minUtility threshold
	 * @return  true if pXY and its extensionsshould be pruned
	 */
	private boolean puPrune(UtilityListHUP X, UtilityListHUP Y, int minUtil) {
            long sum = 0;
            // for each partition 
            for(int i = 0; i < k; i++) {
                    // if Y appear in that partition
                    if(Y.partitionSURU[i] !=0) {
                            // We add the utility + remaining utility of X to the sum
                            sum += X.partitionSURU[i];
                    }
            }
            // if the sum is less than minutil, we can prune pXY and its supersets
            return sum < minUtil;
	}
	//=== END HUP-Miner
        /**
	 * This method constructs the utility list of pXY
	 * @param P :  the utility list of prefix P.
	 * @param px : the utility list of pX
	 * @param py : the utility list of pY
	 * @param minUtility : the minimum utility threshold
	 * @return the utility list of pXY
	 */
        private UtilityListHUP construct(UtilityListHUP P, UtilityListHUP px, UtilityListHUP py, int minUtility) {
            // create an empy utility list for pXY
            UtilityListHUP pxyUL = new UtilityListHUP(py.item,k);
            // BEGIN HUP-Miner
            // Initialize the sum of total utility
            long totalUtility = px.sumIutils + px.sumRutils;
            // END HUP-Miner
            // for each element in the utility list of pX
            for(Element ex : px.elements){
                    // do a binary search to find element ey in py with tid = ex.tid
                    Element ey = findElementWithTID(py, ex.tid);
                    if(ey == null){
                            // BEGIN HUP-Miner
                            totalUtility -= (ex.iutils+ex.rutils);
                            if(totalUtility < minUtility) {
                                    return null;
                            }
                            // END HUP-Miner
                            continue;
                    }
                    // if the prefix p is null
                    if(P == null){
                            // Create the new element
                            Element eXY = new Element(ex.tid, ex.iutils + ey.iutils, ey.rutils);
                            // add the new element to the utility list of pXY
                            pxyUL.addElement(eXY, partitionSize);

                    }else{
                            // find the element in the utility list of p wih the same tid
                            Element e = findElementWithTID(P, ex.tid);
                            if(e != null){
                                    // Create new element
                                    Element eXY = new Element(ex.tid, ex.iutils + ey.iutils - e.iutils,
                                                            ey.rutils);
                                    // add the new element to the utility list of pXY
                                    pxyUL.addElement(eXY, partitionSize);
                            }
                    }

            }
            // return the utility list of pXY.
            return pxyUL;
	}
        /**
	 * Do a binary search to find the element with a given tid in a utility list
	 * @param ulist the utility list
	 * @param tid  the tid
	 * @return  the element or null if none has the tid.
	 */
        private Element findElementWithTID(UtilityListHUP ulist, int tid){
            List<Element> list = ulist.elements;
            // perform a binary search to check if  the subset appears in  level k-1.
            int first = 0;
            int last = list.size() - 1;
            // the binary search
            while( first <= last )
            {
                int middle = ( first + last ) >>> 1; // divide by 2
                if(list.get(middle).tid < tid){
                    first = middle + 1;  //  the itemset compared is larger than the subset according to the lexical order
                }
                else if(list.get(middle).tid > tid){
                    last = middle - 1; //  the itemset compared is smaller than the subset  is smaller according to the lexical order
                }
                else{
                    return list.get(middle);
                }
            }
            return null;
	}
        /**
	 * Method to write a high utility itemset to the output file.
	 * @param the prefix to be writent o the output file
	 * @param an item to be appended to the prefix
	 * @param utility the utility of the prefix concatenated with the item
	 * @param prefixLength the prefix length
	 */
        private void writeOut(String[] prefix, int prefixLength, String item, long utility) throws IOException {
            huiCount++; // increase the number of high utility itemsets found

            //Create a string buffer
            StringBuilder buffer = new StringBuilder();
            // append the prefix
            for (int i = 0; i < prefixLength; i++) {
                    buffer.append(prefix[i]);
                    buffer.append(' ');
            }
            // append the last item
            buffer.append(item);
            // append the utility value
            buffer.append(" #UTIL: ");
            buffer.append(utility);
            // write to file
            writer.write(buffer.toString());
            writer.newLine();
	}
        /**
	 * Print statistics about the latest execution to System.out.
	 */
    public void printStats() {
            System.out.println("=============  HUP-MINER ALGORITHM v0.96r18 - STATS =============");
            System.out.println(" Transaction count: " + transactionCount +
                                            " Partition count: " + k );
            System.out.println(" Partition size: " + partitionSize);
            System.out.println(" Join count: "  + joinCount + " Partial join count:" + partialJoinCount);
            System.out.println(" Total time: " + (endTimestamp - startTimestamp) + " ms");
            System.out.println(" Max. Memory: " + MemoryLogger.getInstance().getMaxMemory() + " MB");
            System.out.println(" High-utility itemsets count: " + huiCount); 
            System.out.println("===================================================");
    }
}
